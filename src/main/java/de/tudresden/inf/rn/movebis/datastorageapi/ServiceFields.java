package de.tudresden.inf.rn.movebis.datastorageapi;

/**
 * 
 * @author Philipp Grubitzsch
 *
 */
public class ServiceFields {

    // CONFIG FIELDS, e.g. from JSON

    /**
     * Config field for temporary upload folder
     */
    public static final String UPLOAD_FOLDER = "uploadFolder";

    /**
     * Path to keystore of own certificates
     */
    public static final String KEYSTORE_FILE_PATH = "keystore_file_path";

    /**
     * Password for keystore
     */
    public static final String KEYSTORE_SECRET = "keystore_secret";

    /**
     * Path to truststore of third party certificates
     */
    public static final String TRUSTSTORE_FILE_PATH = "truststore_file_path";

    /**
     * Password for keystore
     */
    public static final String TRUSTSTORE_SECRET = "truststore_secret";

    /**
     * Port of the service rest endpoint
     */
    public static final String REST_PORT = "http.port";

    /**
     * Host/Domainname of the rest endpoint
     */
    public static final String REST_HOST = "http.host";

    /**
     * Path to the public key pem file
     */
    public static final String PUBLIC_AUTH_KEY_PATH = "auth.publicKeyPath";

}
