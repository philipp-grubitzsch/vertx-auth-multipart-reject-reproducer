package de.tudresden.inf.rn.movebis.datastorageapi;

/**
 * 
 * @author Philipp Grubitzsch
 *
 */
public class DataFields {

    /**
     * object storage: to be used as filename in user folder
     */
    public static final String MEASUREMENT_ID = "measurementId";

    /**
     * object storage: to be used as folder name
     */
    public static final String USER_ID = "userId";

    public static final String LOCAL_FILE_NAME = "localFileName";

}
