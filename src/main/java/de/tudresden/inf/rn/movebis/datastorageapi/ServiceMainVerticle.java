package de.tudresden.inf.rn.movebis.datastorageapi;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.JksOptions;
import io.vertx.ext.auth.KeyStoreOptions;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.auth.jwt.impl.JWTUser;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;

/**
 * 
 * @author Philipp Grubitzsch
 *
 */
public class ServiceMainVerticle extends AbstractVerticle {

    static final Logger logger = LoggerFactory.getLogger(ServiceMainVerticle.class);

    static final String DEFAULT_UPLOAD_FOLDER = "temp-uploads";

    File uploadFolder;
    JWTAuth authProvider;

    private final static String PEM_PREFIX = "-----BEGIN PUBLIC KEY-----";
    private final static String PEM_POSTFIX = "-----END PUBLIC KEY-----";

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        logger.info("Starting Service with config {}", this.config());

        // set local temp upload folder
        uploadFolder = new File(this.config().getString(ServiceFields.UPLOAD_FOLDER, DEFAULT_UPLOAD_FOLDER));

        // general keystore options
        KeyStoreOptions ksOpt = new KeyStoreOptions().setPath(this.config().getString(ServiceFields.KEYSTORE_FILE_PATH))
                .setPassword(this.config().getString(ServiceFields.KEYSTORE_SECRET));

        // setup JWT Authentication
        logger.info("Filename {}", this.config().getString(ServiceFields.PUBLIC_AUTH_KEY_PATH));
        String publicAuthKey = null;
        try {
            publicAuthKey = new String(
                    Files.readAllBytes(Paths.get(this.config().getString(ServiceFields.PUBLIC_AUTH_KEY_PATH))));

            publicAuthKey = (publicAuthKey.split(PEM_PREFIX)[1].split(PEM_POSTFIX))[0];
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        logger.info("Use publicAuthKey {}", publicAuthKey);
        JWTAuthOptions authConfig = new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions().setAlgorithm("RS256").setPublicKey(publicAuthKey));

        authProvider = JWTAuth.create(vertx, authConfig);

        // Create a router object.
        Router router = Router.router(vertx);

        // setup routes for different resources

        // serving basic output
        // router.get("/").handler(routingContext -> {
        // HttpServerResponse response = routingContext.response();
        // response.putHeader("content-type", "text/html")
        // .end("<h1>This is the REST endpoint of the Movebis cloud processing backend</h1>");
        // });

        // POST for upload is only allowed with a valid auth token
        Route route = router.route("/*").handler(JWTAuthHandler.create(authProvider));
        route.failureHandler(authFail -> {
            System.out.println("FAIL");
            authFail.response().close();
        });
        router.route("/measurements").handler(BodyHandler.create().setUploadsDirectory(uploadFolder.getPath()));
        router.post("/measurements").handler(this::storeMeasurement);

        // Create the HTTP server and pass the "accept" method to the request handler.
        logger.info("Starting Webserver...");
        int port = config().getInteger(ServiceFields.REST_PORT, 8090);
        String host = config().getString(ServiceFields.REST_HOST);

        // configure secure connection
        HttpServerOptions httpServerOpt = new HttpServerOptions();
        httpServerOpt.setSsl(true);
        JksOptions jksOpt = new JksOptions();
        jksOpt.setPath(this.config().getString(ServiceFields.KEYSTORE_FILE_PATH))
                .setPassword(this.config().getString(ServiceFields.KEYSTORE_SECRET));
        httpServerOpt.setKeyStoreOptions(jksOpt);
        logger.info("Supported secure transport protocols: {}",
                httpServerOpt.getEnabledSecureTransportProtocols().toString());
        vertx.createHttpServer(httpServerOpt).requestHandler(router::accept).listen(
                // Retrieve the port from the configuration,
                // default to 8080.
                port, host, result -> {
                    if (result.succeeded()) {
                        logger.info("Webserver is listening on port {}", port);

                        // start Object Storage client
                        startFuture.complete();

                    } else {
                        logger.error("Could not start Webserver: {}", result.cause());
                        startFuture.fail(result.cause());
                    }
                });

    }

    /**
     * THIS METHOD SHOULD ONLY BE ENTERED AFTER SUCCESSFUL AUTH check
     * 
     * @param routingContext
     */
    private void storeMeasurement(RoutingContext routingContext) {
        // get user meta data from auth token
        JWTUser user = (JWTUser)routingContext.user();
        JsonObject userMeta = user.principal();

        // get multi attributes and files from request
        MultiMap attributes = routingContext.request().formAttributes();
        Set<FileUpload> uploads = routingContext.fileUploads();
        if (logger.isDebugEnabled()) {
            logger.debug("{} file(s) received with request. Files: ", uploads.size());
            uploads.forEach(uploadedFile -> {
                logger.debug("file name: {}, local temp name: {}, file size: {} Byte", uploadedFile.fileName(),
                        uploadedFile.uploadedFileName(), uploadedFile.size());
            });
        }

        routingContext.response().end();
        ArrayList<String> fileNames = new ArrayList<>();
        ArrayList<String> uploadFileNames = new ArrayList<>();

        // process each uploaded file
        // do something
    }

}
