package de.tudresden.inf.rn.movebis.datastorageapi;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

// Vertx web client API version 3.5.0 does not support multipart fileuploads. To avoid building the necessary
// request manually, the apache http client is used for testing purposes.

/**
 * This Test represents a non-valid upload in terms of a not existing resource but with a valid auth token. The file
 * should not be received in the upload folder.
 * 
 * @author Philipp Grubitzsch
 *
 */
@RunWith(VertxUnitRunner.class)
public class ServiceMainVerticleUploadWrongResourceTest {

    static final Logger logger = LoggerFactory.getLogger(ServiceMainVerticleUploadWrongResourceTest.class);
    Vertx vertx;
    static final String DEFAULT_UPLOAD_TEST_FOLDER = "temp-uploads";
    String testHost = "localhost";
    int testRestPort = 8090;
    String testToken;

    @Before
    public void setUp(TestContext context) {
        System.setProperty("vertx.disableDnsResolver", "true"); // workaround to avoid exception on windows deployments
        vertx = Vertx.vertx();

        JsonObject cfg = new JsonObject();
        cfg.put(ServiceFields.REST_HOST, "localhost");
        cfg.put(ServiceFields.UPLOAD_FOLDER, DEFAULT_UPLOAD_TEST_FOLDER);
        cfg.put(ServiceFields.REST_PORT, testRestPort);
        cfg.put(ServiceFields.KEYSTORE_FILE_PATH, "security/keystore_dev.jks");
        cfg.put(ServiceFields.KEYSTORE_SECRET, "secret");
        cfg.put(ServiceFields.PUBLIC_AUTH_KEY_PATH, "security/public-key.pem");
        DeploymentOptions dOpt = new DeploymentOptions().setConfig(cfg);

        try {
            testToken = new String(Files.readAllBytes(Paths.get(this.getClass().getResource("/accesstoken").toURI())));
            logger.info("Use token {}", testToken);
        } catch (IOException | URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        vertx.deployVerticle(ServiceMainVerticle.class.getName(), dOpt, context.asyncAssertSuccess());
    }

    @After
    public void tearDown(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void testPostMeasurement(TestContext context) {
        final Async async = context.async();

        SSLConnectionSocketFactory sslsf = null;
        try {
            SSLContext sslcontext = SSLContexts.custom().loadTrustMaterial(new File("security/truststore_dev.jks"),
                    "secret".toCharArray(), new TrustSelfSignedStrategy()).build();
            sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] {"TLSv1"}, null,
                    SSLConnectionSocketFactory.getDefaultHostnameVerifier());
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | CertificateException
                | IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        // Allow TLSv1 protocol only
        try (CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build()) {

            File file = new File(this.getClass().getResource("/randomfile1-1mb").toURI());

            // build multipart upload request
            HttpEntity data = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                    .addBinaryBody("upfile", file, ContentType.DEFAULT_BINARY, file.getName())
                    .addTextBody(DataFields.USER_ID, "user1", ContentType.DEFAULT_TEXT)
                    .addTextBody(DataFields.MEASUREMENT_ID, "measurementId1", ContentType.DEFAULT_TEXT).build();

            // ATTENTION: build http(S)!!! request and assign multipart upload data
            HttpUriRequest request = RequestBuilder.post("https://" + testHost + ":" + testRestPort + "/notexisting")
                    .setEntity(data).setHeader("Authorization", "Bearer " + testToken).build();

            logger.debug("Executing request " + request.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    async.complete();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException(
                            "Unexpected response status: " + response.getStatusLine().toString());
                }
            };
            String responseBody = httpclient.execute(request, responseHandler);

            logger.debug("testresponse received: {}", responseBody);
        } catch (IOException | URISyntaxException e) {
            context.fail(e);
        }

    }
}
